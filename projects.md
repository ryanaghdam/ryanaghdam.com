---
title: Projects
layout: page
---

# Projects

[mr-checkbox](https://github.com/ryanaghdam/mr-checkbox) -- Validates a value
against a given set of requirements. Named after a former colleague.  Mr.
Checkbox (the function, not the person) is curried; validators can be created
with partially-applied invocation.

[has-key-deep](https://github.com/ryanaghdam/has-key-deep) -- Deep search
objects for keys in JavaScript objects.  Keys can be searched by providing an
array of keys or using a dot notation.

[propDeep](https://github.com/ryanaghdam/propDeep) -- Returns the given
deeply-nested property of an object using dot-notation or an array of keys.

[wsm-log](https://github.com/ryanaghdam/wsm-log) -- A cooking log intended for
a Weber Smokey Mountain and a dual-probe thermometer.  This is a document
written in LaTeX.

[computer-modern-fonts](https://github.com/ryanaghdam/computer-modern-fonts) --
A Bower package that contains Computer Modern fonts.

[grunt-sibilant](https://github.com/ryanaghdam/grunt-sibilant) -- A Grunt task
that compiles [Sibilant](https://sibilantjs.info/) to JavaScript.  It was not a
serious effort; it was used only to make trying Sibilant easier.  There is only
one commit.  I no longer use Grunt so this is not maintained.

[grunt-mvn-deploy](https://github.com/ryanaghdam/grunt-mvn-deploy) -- A Grunt
task to deploy node projects to Maven repositories. This project was created to
ease a difficult deployment process in a different project.  I do not use Grunt
(nor do I recommend using Grunt); this project is no longer maintained.

[reddits-to-opml](https://github.com/ryanaghdam/reddits-to-opml) -- An old
(August 2010) Python project that produces an OPML feed of subscribed
subreddits for a given Reddit user.
