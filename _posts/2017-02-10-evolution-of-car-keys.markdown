---
title: The Evolution of Car Keys
category: cars
layout: post
excerpt: In the past half century car keys have changed a lot. Has it been for the better?
---

Awhile ago eBay suggested that I buy a vintage Toyota key.  For reasons I am
unable to explain, I decided that I would much rather have a key to an unknown
1960's Toyota than $2.  I keep that key on the ring next to my FR-S key.

The shape is the same.  But that's where the similarities end.  In the past
half century keys have changed a lot.  Has it been for the better?

{% include image.html image="IMG_5767.jpg" %}

The old key is two inches long and three quarters of an inch wide; the new key
is three and a half inches long, one and a half inches wide, and -- most
importantly -- a half inch thick.

*Old keys: 1; new keys: 0.*

That bulk must be there for a reason.  On this particular key, it houses the
remote for locking/unlocking the doors, opening the trunk, and setting off the
alarm.  Is that really much more convenient than using a key to lock/unlock the
door?  In my beloved and dearly missed 2001 Nissan Altima, no.  When
locking/unlocking the driver's door with the key it'd operate the locks on all
of the doors.  The FR-S doesn't do that, which makes those buttons somewhat
necessary if you have a passenger.  A point to the new keys.

*Old keys: 1; new keys: 1.*

But what about that button that sets off the alarm?  That button's been pressed
many times -- never once intentionally.

*Old keys: 1; new keys: 0.*

Almost immediately after buying my 1991 Miata, I decided it would be wise to
make a spare copy of the key because it came with only one master and one
valet.  Easy.  A quick ride to the hardware store and five minutes and $4
later, I had two spares.

{% include image.html image="IMG_5770.jpg" %}

Hannah's RAV-4 needed a key copied, too. We needed a spare because "I" lost her
keys.  The RAV-4's key are nearly idential to those for the FR-S.  They have a
transponder and remote, so I expected it to be a bit more expensive, but not
inconvenient.  I didn't care about the remote, just a key that starts the car.
The hardware store that copied my Miata key claimed to do "chipped" keys so I
tried them first.  They couldn't do this one.  Nor could the other hardware
store in town.  Not Lowe's either.  The locksmith that Lowe's suggested?  Nope.
I eventually had luck at [Sears][0], where they made a copy for $50.

[0]: https://www.yelp.com/biz/the-keyless-shop-at-sears-burlington

*Old keys: 2; new keys: 0.*

Transponders were added to reduce rampant car theft.  Since transponders have
become nearly ubiquitous, car theft rates have plummeted.  They're clearly
effective, if sometimes frustrating.

*Old keys: 2; new keys: 1*

The reduced rates of car theft can often lead to discounts on car insurance,
which is further proof of their effectiveness.

*Old keys: 2; new keys: 2*

But, I'd risk the chance at having Hannah's RAV-4 stolen rather than spend two
hours trying to have a key copied.  I'll keep the transponder for my cars,
though.

Oh, and if you're about to take a picture of your plain old metal keys and drop
them on a plate covered in barbecue sauce, you can just run them under the sink
for a minute.  Another point for the old keys.
