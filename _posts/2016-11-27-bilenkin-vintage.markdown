---
layout: post
title: "Interesting Car: The Bilenkin Vintage"
categories:
  - cars
---


For the modest amount of $140,000, a Russian coach building company, Bilenkin,
will turn your E92 BMW M3 Coupe into a 1960's era vintage GT car.  Bilenkin
boasts that they combine 1960's styling and elegance with modern comfort
and reliability.

{% include image.html image="bilenkin-vintage-015-1.jpg" caption="A conceptual sketch from Bilenkin.  The dapper owner of this Vintage packs his trunk with stylish luggage for a trip with this fetching lady.  His neighbor, a lonely man in plain clothes, trailers his black 1970's BMW to the back of his grey X5." %}

1960's automotive styling is revered and considered to be the peak of
automotive design because it was, well, the peak of automotive design.  It was
the end of the era where cars were designed only by artists instead of artists
and bureaucrats.  Automotive regulations constrained artists in building what
they truly wanted.  They had to consider requirements for bumpers and other
nonsense.

{% include image.html image="bilenkin-vintage-001-1.jpg" caption="Front three quarters view of the Bilenkin Vintage" %}

{% include image.html image="bilenkin-vintage-004-1.jpg" caption="Rear three quarters view of the Bilenkin Vintage" %}

Does the Vintage capture that elegant 1960's design?  Kind of.  It looks like
it's from that era, but it isn't elegant like the stand-out cars of that era.

That's because it seems to have been styled by a Russian James May.

{% include image.html caption="Yakov May (left) showing off his creation to the media."  image="bilenkin-yakov.jpg" %}

The interior is garish.  It's a hodgepodge of 1960's (and 1950's) materials and
trends stuck on top of a modern BMW.  There's a lot of burled walnut (which I
like), lots of teal and cream colored accents, mother of pearl trim, crystal,
and a lot of Paisley.

{% include image.html image="bilenkin-vintage-007-1.jpg" caption="Here's what your passengers will be looking at." %}

{% include image.html image="bilenkin-vintage-006-1.jpg" caption="Yes, the seats are paisley." %}

{% include image.html image="cold-car-vintage-moscow.jpg" caption="The HVAC controls aren't that bad." %}

{% include image.html image="111_Bilenkin-classic-cars.jpg" caption="The gauge cluster" %}

The Bilenkin Vintage looks very well made.  However, combining every single
design element from an era of elegant design does not result in an elegant
design.

{% include image.html image="screen-shot-2014-12-27-at-2.11.1320PM.png" caption="See?" %}

