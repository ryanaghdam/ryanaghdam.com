---
title: The Art of the Ultimate Driving Machine
category: cars
layout: post
excerpt: Who runs BMW these days?  The marketing department or the engineers?
---

I recently ditched my multi-blade Gillette razor for a safety razor. A
bit unfamiliar to single-blade shaving, I decided to visit The Art of
Shaving on a trip to the mall. Upon entering, you're greeted by an
overeager salesman with patchy stubble who tries to sell you a product
using words like "premium" and "luxury", without describing actual
characteristics. From what I could tell, a majority of the products
were simply Gillette products with fancy-looking handles. The products
are designed to make users feel like they're high-quality products
made by the craftsmen of yesteryear.

Unimpressed, I left en route to Chick-fil-A for a spicy chicken
sandwich. I stopped on the way to look at a BMW X4 that was on
display, mostly because I didn't know the X6 had spawned a
slightly-smaller but just-as-ugly twin. There were a few bits of
literature with many roundels and the world "ultimate".  The only
piece of literature to mention any aspects of the drivetrain was the
Monroney.

Being a new and unique body style, I was expecting that some of the
advertising materials would explain the why this car is any good.
More importantly, what makes it an ultimate driving machine?  What
does this vehicle do better than any other?  Can it take you through
terrains that others cannot?  Can it handle the the 154 turns of the
Nordshleife with ease?  Can it take five passengers in comfort?  Or is
it simply a good value for money?

These are questions that should be answered, especially when you use
the word "ultimate" to describe a product and dare to ask potential
buyers for fifty thousand hard-earned dollars.  In the era where BMW
was a brand that I liked and respected, these questions would have
certainly been answered.

Until driving an E46, I wasn't able to appreciate or even understand
why BMW did claim make the ultimate driving machine.  Sure, I thought
they looked nice; very elegant and subtle.  Wood and leather made the
it a pleasant place to be a passenger. Hop over one seat to the left
and you're in for a real treat.

Every single review I've read of the X6 and the X4 indicates that
these so-called Sports Activity Vehicles have very little in common
with the E46 (and E39) that I respect (and want).  It seems that they
were designed by a committee based on the answers to surveys.  Surveys
that were answered by people who might even tell you that their
5-series has a V-6. I get the feeling that whatever consumers said
they liked, the designers and engineers tried to incorporate, without
considering if they were qualities that worked well together.

I get the impression that twenty years ago, a few Bavarian men and
women drove their test mules around and smiled as they tried out the
handling capabilities and ride comfort.  Now, I get the impression
that some marketing folks look at a spreadsheet through designer
eyeglasses and are delighted when they can tick off as many boxes as
possible.

The Art of Shaving and modern BMW do not strive to sell excellent
niche products; they strive to sell the experience of a crafted,
specialized product to the mass market.  It's nothing more than a
facade; just a pseudo-ivory handle or a blue and white roundel.
Chick-fil-A, however, does exactly what they advertise: sell damn-good
chicken sandwiches at a reasonable price.

**Appendix A: reasons to still respect BMW**

- BMW refuses to make a V6; the straight-6 is superior.

- BMW is one of the few manufacturers to offer a diesel option for
  some of its vehicles.

- BMW still sells a proper wagon!

- BMW will still let you configure nearly every model (except those
  diesel wagons!) with a manual transmission.


This is also posted on [OppositeLock](http://oppositelock.kinja.com/the-art-of-the-ultimate-driving-machine-1683124369)
