---
title: Car Spotting in Palo Alto
category: cars
layout: post
---

Walking around Palo Alto, CA, I found a second generation (1953-56) Ford F-Series.

{% include image.html image="IMG_0385.jpg" caption="Second generation Ford F-Series, front view" %}

{% include image.html image="IMG_0384.jpg" caption="Second generation Ford F-Series, three quarters side view" %}


{% include image.html image="IMG_0387.jpg" caption="Second generation Ford F-Series, badging" %}

{% include image.html image="IMG_0388.jpg" caption="A defaced picture of Pablo Sandoval hanging in the men's room.  You can have him back, California." %}
