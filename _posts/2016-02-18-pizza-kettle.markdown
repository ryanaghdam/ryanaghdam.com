---
title: Pizza Kettle Maiden Voyage
layout: post
category: meat
---

My parents bought me a Pizza Kettle for Christmas.  I was sick of waiting for
spring, so I took it out on the first warm-ish Saturday.


{% include image.html image="IMG_0221.jpg" caption="The setup. It was about 40 degrees outside -- the warmest weekend in awhile." %}

{% include image.html image="IMG_0224.jpg" caption="The first pizza.  Certainly some room for improvement: we stretched the dough a bit too thin, there was too much pepperoni which made it a bit too greasy." %}


{% include image.html image="IMG_0230.jpg" caption="The second one in the oven." %}


{% include image.html image="IMG_0222.jpg" caption="Once heated, the temperature rose to quadruple digits, pushing the needle almost all the way around." %}


{% include image.html image="IMG_0227.jpg" caption="The second pizza out of the oven." %}

