---
title: iPhone Pocket Lint
layout: post
category: misc
---

The volume controls on my headphones have recently stopped working when
plugged into my iPhone.  I had also noticed that the headphones were becoming
unplugged far too easily.

I thought that something might be stuck in the headphone jack so I cleaned it
out with a toothpick and a sewing needle.

Turns out, there was a lot of pocket lint in there.


{% include image.html image="IMG_0052.jpg" caption="Lint removed from iPhone headphone jack with key and Swiss Army Knife for scale." %}
