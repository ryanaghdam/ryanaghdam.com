---
title: "On Don, Gone"
category: baseball
layout: post
date: 2015-08-31
excerpt: "Earlier this week, the news of long-time Red Sox broadcaster Don Orsillo's impending departure leaked.  Orsillo's contract, which expires at the end of this season, will not be renewed by NESN.  According to The Boston Herald, Tom Werner has decided to \"reenergize\" the broadcast to combat low ratings.  Dave O'Brien, the radio voice of the Boston Red Sox, is replacing Don Orsillo."
---

Earlier this week, the news of long-time Red Sox broadcaster Don Orsillo's impending departure leaked.  Orsillo's contract, which expires at the end of this season, will not be renewed by NESN.  According to The Boston Herald, Tom Werner has decided to "reenergize" the broadcast to combat low ratings.  Dave O'Brien, the radio voice of the Boston Red Sox, is replacing Don Orsillo.

The news hit hard for many reasons.  The first is that the announcer will be _different_ for the first time in 15 years.  Since Orsillo started, we -- Red Sox fans, that is -- have some good memories, starting with Hideo Nomo's no-hitter on Don's first day.  Three no-hitters would follow from Derek Lowe, Jon Lester, and Clay Buchholz.  There was the Mother's Day Miracle, a few games to clinch a postseason berth, and the list goes on and on.  Most importantly, Don and Jerry have been present night after night, through the mundane and meaningless games, too.  Don and Jerry have been a staple throughout my home over these years.  They're at our dinner table nearly every night, like part of the family.

The chemistry that the two have is something special.  They genuinely like each other and are able to showcase their personalities in a way that is appropriate for the game.  A blowout may lead to a lesson on windshield wipers and high beams, or walkthrough of Don's bag.

Don has been a crucial part of Red Sox Nation.  Tom Werner does not realize what it's like to be a fan. We're not at Fenway every night.  Each night we tune into Don and Jerry and cheer for our team, through the good and bad.  They're our gateway to the Sox.

I remember going to my first Red Sox game when I was about 10 or so.  After we settled in to our bleacher seats, I felt that I was missing out on what Don and Jerry had to say.  I don't deny that Tom Werner is a successful television executive; he's responsible for _The Cosby Show_ and _Roseanne_.  He understands how to make television for a national audience.  As for Red Sox baseball, he doesn't "get it".  Red Sox fans -- New Englanders -- we're different.

We accept our own kind.  We like right fielders with dirty hats (even if they're from Durham, NC), pitchers who limp to the mound minutes after a surgery (Ankorage, AK), pitchers who sacrifice a post-season start to preserve the bullpen (Melborne, FL), catchers who catch four no hitters and punch Alex Rodriguez in the face (Rochester, MI), starters who pitch six perfect innings from the bullpen with a shoulder injury (Manoguayabo, DR), shortstops turned managers turned broadcasters turned Fenway fixture (Portland, OR), designated hitters who produce in the clutch (Santo Domingo, DR), the greatest hitter who ever lived (San Deigo, CA), triple crown winners who spend an entire 23-year career with the Sox (Southhampton, NY), gentlemen and 8-time All-Stars (Anderson, SC).

So who better to call the games than someone who grew up in Madison, NH?  Someone who, as a child, rearranged his bedroom furniture to feature a tall left-field wall, a triangle in center, and a uniquely-shaped right field -- all so he could pretend that he was broadcasting for his favorite team.

Tom Werner's suggestion that a decline in ratings are related to the the broadcast is either woefully ignorant or a baldfaced lie.  If not for Don and Jerry, fewer would be tuning in to watch the worst pitching staff in the American League, the worst left fielder of all time, and a bullpen that inspires the confidence of a Yugo.  The ratings have dropped because the team is painful to watch and has been out of contention for months.

Those of us who have been fans before late-October 2004 are accustomed to losing games in the worst way imaginable, but losing Don is different.  It's like having a close friend and neighbor move away.  This one stings badly and in a way different from Aaron Boone's home run. It's different because it was perpetrated from within, by an out-of-touch, ratings chasing TV executive (New York, NY).
