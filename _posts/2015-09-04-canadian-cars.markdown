---
title: Car Spotting in Nova Scotia
date: 2015-09-05
layout: post
category: cars
excerpt: A few cars spotted in Canada that are not available in the United States
---


{% comment %}
  {% include image.html image="ct2aa.jpg" caption="The Cabot Trail" %}
{% endcomment %}

On a road trip through Nova Scotia, I noticed a few cars on the road that are not available for purchase in the United States.

- The [Acura CSX](https://en.wikipedia.org/wiki/Acura_CSX) is an entry-level Acura built for the Canadian market in Canada.  I only saw a few of these.  Luxury-branded vehicles, most of which had plates from Ontario, were few and far between.

- Replacing the Nissan Versa sedan is the [Nissan Micra](https://en.wikipedia.org/wiki/Nissan_Micra#Canada).  The base model with a five-speed starts under $10,000 CAD.  I saw many Micras on the road in parked in driveways.

- Nissan [X-Trail](https://en.wikipedia.org/wiki/Nissan_X-Trail#Second_generation_.282007.E2.80.932013.29), the predecessor to the Nissan Rogue.  These are getting a bit on in age so there were only a few.  I'd hazard a guess that most of them have rusted away in this climate.

- [Mercedes B Class](https://en.wikipedia.org/wiki/Mercedes-Benz_B-Class#First_generation_.28T_245.2C_2005.E2.80.932011.29), if you ignore that the electric drive version is available in the US.

- The [Mitsubishi RVR](https://en.wikipedia.org/wiki/Mitsubishi_RVR#Third_generation_.282010.E2.80.93present.29), which is really just an Outlander.  On a related note, there are more Mitsubishis driving around out here than there are back home.  I would guess that Mitsubishi does well in Canada because they offer long-term financing.  If I search for current offers for Mitsubishi, they're offering 0% APR for 72 months on an RVR.

- The [Pontiac Wave](https://en.wikipedia.org/wiki/Chevrolet_Aveo), which I think is a Chevy Aveo.

{% comment %}
  {% include image.html image="IMG_0152.jpg" caption="Parked on observation area on the Cabot Trail" %}
{% endcomment %}

{% comment %}
  {% include image.html image="IMG_2144.jpg" caption="540 miles into the road trip.  The ability to switch units on the speedometer came in handy." %}
{% endcomment %}

A few other observations:

- There are lots of compact and sub-compact sedans and hatchbacks (like the Toyota Corolla and Nissan Micra) and full-size pickup trucks.  I didn't see very many mid-size sedans.

- Dodge seems to be a more popular brand here than in Massachusetts.  This contributed to my hypothesis about Mitsubishi's popularity being related to long-term financing.

- Nova Scotia does not require front license plates.

- Speed limits on the Trans-Canada Highway vary frequently and in 10 km/h increments between 70 and 110 km/h.

- Google Maps has a Canadian accent in Canada.  She says "roundaboooot".

{% comment %}
  {% include image.html image="IMG_0131.jpg" caption="The coast" %}
{% endcomment %}
