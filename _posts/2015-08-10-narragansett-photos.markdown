---
title: Photos from Narragansett
layout: post
category: misc
---

A few photos from this weekend at Narragansett.

{% include image.html image="IMG_0055.jpg" %}
{% include image.html image="IMG_0057.jpg" %}
{% include image.html image="IMG_0063.jpg" %}
{% include image.html image="IMG_0065.jpg" %}
{% include image.html image="IMG_0054.jpg" %}
