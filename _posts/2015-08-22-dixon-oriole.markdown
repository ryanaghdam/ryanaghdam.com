---
title: "Pencil Review: Dixon Oriole"
layout: post
category: pencils
---

Do you remember anything front your childhood differently than you see it now?  I remembered Rocco's Modern Life as a hilarious show about a weird wallaby who hated his job. Now, it's more apparent that he worked as a phone sex operator.  Whenever a Dixon Oriole found its way into my desk in grammar school, I was pleased.

Now that I found others who share my odd interest in pencils, I started to slowly build up a collection.  With an Amazon gift card burning a hole in my digital wallet, I browsed their collection of woodcase pencils and excitedly added a 12-pack of Dixon Orioles, for $2.

Two days later I opened the box and freed the first pencil from its package.  As I pulled it out, I remembered the shade of yellow being slightly different.  Maybe colors in the late 1990's were different.  I could have sworn that the paint in the embossment was supposed to be silver -- not gold.  The presharpened point looked a bit lopsided, but I was distracted by the splintered wood.

Maybe they write well.  No.  Erase well? Nope.

These aren't the Dixon Orioles I remembered, so I checked the packaging more closely.

"Made in India".

Another great American-made pencil that has gone, to return only in name, disguising a low quality pencil.  This is like replacing Cal "The Iron Man" Ripkin Jr. with Nomar "The Glass Man" Garciaparra.
