---
title: Basic Internet Security
category: software
excerpt: A guide to basic internet security and password management
layout: post
---

How does someone get hacked?

Here's a common scenario which uses a sweet little old lady to toy with your
emotions.  You're playing the role of the sweet little old lady in this one.

1. You use your email address, littleoldlady@aol.com, to sign up for Bank of
   America, PayPal, and ArthriticKnitting.com.

2. It's hard to remember more than one password.  Heck, it's hard for you to
   remember what medication you're supposed to take.  So you're using the same
   password, `ilovemygrandson`, for all of these websites.

3. ArthriticKnitting.com is hacked.  The attackers sell the list of email
   addresses and passwords to other hackers.

4. Whomever buys the list of email addresses and passwords tries to login to
   PayPal with your email address and password and is able to send himself some
   money.

5. The attacker has found a working credential and will try to use that
   information on other sites, too.  He finds that it also works on
   BankOfAmerica.com.


## Managing Passwords

### Unique Passwords

Do not use the same password across multiple websites.

When a website is hacked a list of usernames and passwords are often sold.
Hackers buy these lists to see which username and password combinations work on
other websites.  It's not a big deal if your password on something like a car
forum is hacked.  If that's also the password you use for online banking, then
you *must* consider that account hacked as well.


### Random Passwords

Use a randomly-generated combination of letters, numbers, and symbols.  Do not
use your name or any words found in a dictionary.

Try a random password generator, if you want help.  You'll end up with
something crazy like this: `}sKj^&@'>H<2}Qneq^mP%` but it's more secure.

Frequently used passwords are often tried when attacking an account.  Hackers
can attempt to automate attacks using lists of frequently used passwords and a
dictionary.


### Keep Track

Keep track of your passwords. There are many software solutions including 1
Password, LastPass, and Dashlane.  You can even use a sheet of paper, if that's
what you'd prefer.

The best solution is the one that you'll actually use.
