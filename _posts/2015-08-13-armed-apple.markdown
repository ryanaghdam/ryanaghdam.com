---
title: Is Tim Cook Packin' Heat?
layout: post
category: misc
excerpt: The author of a post on Gear Patrol feels that sourcing materials should't be difficult for a "powerful and wealthy" company.  Why is he wrong?
---

A recent post on Gear Patrol titled ["Why I Bought the Last iPod Classic in the Store"][Article] describes that the product was discontinued after "it had simply become too hard to source the materials needed to make the Classic".  The author, Greg Goldstone, continues: "it seemed hard to believe that a company as powerful and wealthy as Apple could meet such a trivial roadblock in production".

Why is Apple "powerful"?  Does Tim Cook go to business meetings with a Beretta 92 FS Inox tucked into his pants, revealing it with a finessed shirt adjustment whenever his supplied says something disapointing?  No, he probably just pulls out a spreadsheet of sales projections and assures the supplier that a certain quantity of material will be purchased.  Hopefully, that quantity justifies the a low cost that will keep the supplier profitable.  It's the needs and wants of Apple's many millions of loyal customers who are powerful.

Goldstone also mentions that Apple is a wealthy company.  Sourcing materials shouldn't be a problem.  This is far more ignorant than assuming Tim Cook stops off at the Apple Armory before making a deal.  It's no secret that Apple has some cash lying around.  Why not use it to buy some iPod Classic materials?  Well, the demand for the iPod Classic is low, even as its price has fallen over the years. (Ignore the blip in demand due to nostalgia.)  With lower demand, per-unit costs increase as smaller economies of scale are achieved.  If material costs are higher, as explained by the author, that further increases the cost of producing a single iPod Classic.  It's clear that Apple could lose money if they were to continue to sell iPod Classics.

Why bother selling an old product at a loss when your company focuses on groundbreaking technological innovations?  There is no logical justification for expending resources (opportunity cost) to lose money.  A lack of profitability shows that the cost of the inputs (engineering, raw materials, factory time, shipping, retail space, etc.) exceed the value of the output (an iPod Classic).

It is not unusual to feel nostalgic about a since-forgotten product when the end of its life is announced.  Nostalgia is not a reason to ignore the signals of the profit and loss system.

[Article]: http://gearpatrol.com/2015/08/12/ipod-classic-retrospective/
[92FSInox]: http://www.beretta.com/en-us/92-fs-inox/
