---
title: Free Lobster Roll, sort of
layout: post
category: misc
---

Hannah and I finally mustered the courage to spend $20 to try a lobster roll, so we split one as an appetizer.  I really enjoyed it; I shouldn't have waited to so long to try one.  Best of all, it didn't even cost me anything.

The bottom of my glass fell out, resulting in a torrent of Diet Coke all over my pants.  I looked like I had an incontinence problem, so we took our meal to go.  The waitress was even nice enough to comp the lobster roll, even though I didn't suggest it.

{% include image.html image="IMG_0016.jpg" caption="The receipt." %}

Not too bad.
