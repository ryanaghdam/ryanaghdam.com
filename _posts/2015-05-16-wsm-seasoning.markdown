---
title: Weber Smokey Mountain Seasoning
layout: post
category: meat
---

It's commonly thought that seasoning a smoker is necessary before cooking.  In
the case of the Weber Smokey Mountain (and many other consumer-grade smokers)
seasoning is not required because it is made of steel with a porcelain enamel
finish.  It is only raw steel smokers that require seasoning.

Because it was my first time using a smoker, I did a trial run without any meat
to learn how to regulate and maintain temperature.

Here's the process I followed:

First, ignite 40 briquettes in a chimney.

{% include image.html image="IMG_1642.jpg" caption="Briquettes lit in an chimney.  This particular chimney was on sale at Target for $4.  Invest in a Weber chimney instead." %}

Next, place 40 briquettes in the base of the smoker in a circle, leaving the
middle open.  The technique is called the Minon method.  The briquettes on the outer part of the smoker are lit slowly, maintaining the temperature for several hours.

{% include image.html image="IMG_1629.jpg" caption="Briquettes arranged in a circle, ready to be lit." %}

After the briquettes in the chimney are ignited, place them inside the 40 briquettes in the base of the smoker.

Monitor the temperature, experimenting with the vents to help get a feel for regulating your smoker's temperature.  Keep an eye on the temperature and keep
notes.

{% include image.html image="IMG_1633.jpg" caption="Logging temperatues for future reference." %}

After a bit of experimenting with temperature regulation, I had a feel for what
was necessary to maintain and change temperature.
