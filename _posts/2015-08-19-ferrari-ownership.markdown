---
title: On Ferrari Ownership
category: pencils
layout: post
---

Ferrari is the most iconic manufacturer of sports cars.  Their bright red thoroughbreds are recognizable by nearly everyone, including those who are unable to distinguish between an M5 and a 320i.  Nearly every Ferrari produced has attained high status as a luxury item as well as a properly respectable bit of engineering.

Despite their abilities on the road and track, Ferraris spend most of their time covered in a garage plugged into battery tenders.  The Ferrari owner must wait for the perfect situation to drive: weather, traffic, destination, etc.  And, assuming all of those are good, he must then determine depreciation costs.  Is the drive to the beach worth the additional 200 miles on the clock?   Those miles add up.  Ferraris lose their value rapidly as they approach 40,000 miles.

The joy in owning a Ferrari is much like a prized trophy, a symbol of hard work and success.  And, for some, there is the enjoyment in wearing a Ferrari shirt or those heinous Ferrari-branded Puma sneakers, just hoping someone will point out that you look like a dork and that you probably don't even own a Ferrari.  But then, you can say that you do own a Ferrari, and even pull out your big red key.

Ferrari ownership is difficult because you must decide between enjoying something near perfection and preserving that limited resource.  Trust me, I know what it's like.

I don't own a Ferrari, but I did receive something quite similar for my birthday: a few packages of original (pre-Papermate) Mirado Black Warriors.  These pencils are carefully crafted and supremely smooth.  Much like the hand-built Ferrari 250 GTO, these pencils are a bygone work of art.  The pencil sharpens perfectly evenly -- the lead is dead on center.  The branding is printed and embossed with precision -- the paint is found only in the embossment. The paint doesn't hastily run into the ferrule, which has a perfectly even red stripe.

{% include image.html image="bw1.jpg" caption="The tip of a pre-Papermate Mirado Black Warrior (top) and the tip of a Mexican-made Papermate Mirado Black Warrior (bottom)" %}

These details are no longer found on the Mirado Black Warrior, which is now manufactured by Papermate in Mexico.

{% include image.html image="bw2.jpg" caption="A comparison of the labels on a modern Black Warrior (top) and a classic one (bottom).  Chipping paint is visible on the modern pencil, while the embossment is crisp on the classic pencil." %}

These classic Black Warriors are precious like the Ferraris of the 1960s.  They're used with care and only on certain occasions.  I want to carry one in my pocket for every day use.  But I can't bring myself to do it.  I usually go through a pencil every week and a half.  I wouldn't want to use up the few that I have; I may never have the chance to buy more.  So my Black Warriors sit idle in their packages on the shelf next to my desk.

For years I thought that it was silly that Ferraris sat idle for weeks on end.  I am now able to empathize, thanks to a few leftover packages of pencils.
